package com.example.quanlysachandroi;

public class Book {
    String name,describe,category;
    int img,price;
    public Book(String name, String describe, String category, int img, int price) {
        this.name = name;
        this.describe = describe;
        this.category = category;
        this.img = img;
        this.price = price;
    }
    public Book(){}
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescribe() {
        return describe;
    }
    public void setDescribe(String describe) {
        this.describe = describe;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public int getImg() {
        return img;
    }
    public void setImg(int img) {
        this.img = img;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
}
