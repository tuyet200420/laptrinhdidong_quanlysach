package com.example.quanlysachandroi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

public class book_Adapter extends BaseAdapter {
    Context context;
    int  layout;
    public book_Adapter(Context context, int layout, List<Book> books) {
        this.context = context;
        this.layout = layout;
        this.books = books;
    }
    List<Book> books;
    @Override
    public int getCount() {
        return books.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(layout,null);
        DecimalFormat formatter = new DecimalFormat("###,###,###.##");
        ImageView imageView = convertView.findViewById(R.id.imageSach);
        TextView nameBook=convertView.findViewById(R.id.tvTenSach);
        TextView priceBook=convertView.findViewById(R.id.tvGia);
        TextView priceLoai=convertView.findViewById(R.id.tvTheLoai);
        imageView.setImageResource(books.get(position).img);
        nameBook.setText(books.get(position).name);
        priceBook.setText(formatter.format(books.get(position).price)+" VNĐ");
        priceLoai.setText(books.get(position).category);
        return convertView;
    }
}
