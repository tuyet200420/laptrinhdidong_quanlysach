package com.example.quanlysachandroi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class productDetail extends AppCompatActivity {
    Button btn_back;
    TextView title,price,describe,category;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        btn_back=(Button)findViewById(R.id.btn_back);
        title=(TextView)findViewById(R.id.titleDetail);
        price=(TextView)findViewById(R.id.priceDetail);
        describe=(TextView)findViewById(R.id.describeDetail);
        category=(TextView)findViewById(R.id.cateDetail);
        imageView=(ImageView)findViewById(R.id.imageDetail) ;
        Intent intent = this.getIntent();
        DecimalFormat formatter = new DecimalFormat("###,###,###.##");
        if (intent != null){

            String txtname = intent.getStringExtra("name");
            String txtdescri = intent.getStringExtra("describe") ;
            String txtcategory = intent.getStringExtra("category");
            int txtImg=intent.getIntExtra("img",R.drawable.sach5);
            int txtPrice=intent.getIntExtra("price",1000000);
            String gia=formatter.format(txtPrice)+" VNĐ";
            title.setText(txtname);
            describe.setText(txtdescri);
            category.setText(txtcategory);
            price.setText(gia);
            imageView.setImageResource(txtImg);
        }
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}