package com.example.quanlysachandroi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button bt_login;
    TextView txt_signup,textvali;
    EditText email,password;
    acount acountList = new acount();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt_login=(Button)findViewById(R.id.buttonLogin);
        txt_signup=(TextView)findViewById(R.id.txtSignUp);
        email=(EditText)findViewById(R.id.inputEmail);
        password=(EditText)findViewById(R.id.inputPassword) ;
        textvali=(TextView)findViewById(R.id.textVali);

        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=getIntent();
                String dl_mail=i.getStringExtra("mail").trim();
                String dl_pass=i.getStringExtra("pass").trim();
                String txt_mail = email.getText().toString().trim();
                String txt_pass=password.getText().toString().trim();
                if(txt_mail.isEmpty() || txt_pass.isEmpty()){
                    textvali.setText("Không được để trống");
                }
                else {
                    if((txt_pass.equals(dl_pass)) && (txt_mail.equals(dl_mail))){
                        textvali.setText("");
                        Toast.makeText(getApplicationContext(),"Đăng nhập thành công",Toast.LENGTH_SHORT).show();
                        Intent listItem=new Intent( MainActivity.this, product_list.class);
                        startActivity(listItem);
                    }
                    else
                        textvali.setText("Mật khẩu hoặc pass sai");
                }
            }
        });
        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent( MainActivity.this, sceen_sign_up.class);
                startActivity(i);
            }
        });
    }
}