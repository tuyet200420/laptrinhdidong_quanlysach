package com.example.quanlysachandroi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class sceen_sign_up extends AppCompatActivity {
    TextView txt_sigin,validPass;
    Button Bt_sigup;

    EditText name,pass,confimPass,mail;
    String txtName,txtPass,txtConfrim,txtMail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sceen_sign_up);
        acount ac1 = new acount();
        validPass=(TextView)findViewById(R.id.text_confirmPass);
        txt_sigin=(TextView)findViewById(R.id.txtSignin);
        mail=(EditText) findViewById(R.id.inputEmail);
        name=(EditText)findViewById(R.id.inputName);
        confimPass=(EditText)findViewById(R.id.inputConfirmPass);
        Bt_sigup=(Button) findViewById(R.id.buttonSignup);
        pass=(EditText)findViewById(R.id.inputPasswprd);
        Bt_sigup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Bundle bundle=new Bundle();
                String pass1=pass.getText().toString().trim();
                String pass2=confimPass.getText().toString().trim();
                txtName = name.getText().toString();
                txtPass = pass.getText().toString().trim();
                txtMail=mail.getText().toString();
                if(txtName.isEmpty() || txtPass.isEmpty() || txtMail.isEmpty() || pass2.isEmpty()){
                    validPass.setText("Không được để trống");
                }
                else {
                    if (pass1.equals(pass2)) {
                        ac1.setName(txtName);
                        ac1.setPass(txtPass);
                        ac1.setEmail(txtMail);
                        ac1.addAray(ac1);
                        validPass.setText("");
                        Toast.makeText(getApplicationContext(), "Đăng ký thành công", Toast.LENGTH_SHORT).show();
                    } else
                        validPass.setText("Mật khẩu nhập lại không khớp");
                    }
                }
        });
        txt_sigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(sceen_sign_up.this,MainActivity.class);
                i.putExtra("mail",ac1.getEmail());
                i.putExtra("pass",ac1.getPass());
                startActivity(i);
            }
        });

    }
}