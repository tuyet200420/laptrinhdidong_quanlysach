package com.example.quanlysachandroi;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class product_list extends AppCompatActivity {
    ListView lv;
    ArrayList<Book> arrayList;
    book_Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        lv = (ListView) findViewById(R.id.lvBook);
        arrayList=new ArrayList<>();
        String[]name={"Conquer the demon in you", "Tôi là một con lừa","Bố già","Hai Số Phận","Bầy Rùa Chồng Chất"};
        String[]describe={"Nội dung của câu chuyện xoay quanh cuộc nói chuyện giữa Naponeon Hill với con quỷ. Con quỷ không biết làm cách nào mà có thể kiểm soát được con người về tâm trí.",
                "Nằm trong top những cuốn sách nên đọc trên đời, “Tôi là một con lừa” đã tái hiện thật chân thực, rõ nét hình ảnh người phụ nữ năng động, hiện đại, sống hết mình.",
                "Đây là câu chuyện sinh động và đầy lôi cuốn về thế giới ngầm Mafia ở xứ Sicily. Những cuộc làm ăn bất chính diễn ra trên “mảnh đất màu mỡ” với những khoản lợi nhuận khổng lồ trong thế giới  ngầm.",
                "“Hai số phận” không chỉ đơn thuần là một cuốn tiểu thuyết, đây có thể xem là \"thánh kinh\" cho những người đọc và suy ngẫm, những ai không dễ dãi, không chấp nhận lối mòn.\n" +
                        "\n" +
                        "“Hai số phận” làm rung động mọi trái tim quả cảm, nó có thể làm thay đổi cả cuộc đời bạn. Đọc cuốn sách này, bạn sẽ bị chi phối bởi cá tính của hai nhân vật chính, hoặc bạn là Kane, hoặc sẽ là Abel, không thể nào nhầm lẫn. Và điều đó sẽ khiến bạn thấy được chính mình.\n" +
                        "\n" +
                        "Khi bạn yêu thích tác phẩm này, người ta sẽ nhìn ra cá tính và tâm hồn thú vị của bạn!\n" +
                        "\n" +
                        "“Nếu có giải thưởng Nobel về khả năng kể chuyện, giải thưởng đó chắc chắn sẽ thuộc về Archer.” - Daily Telegraph\n" +
                        "\n" +
                        "“Hai số phận” (Kane & Abel) là câu chuyện về hai người đàn ông đi tìm vinh quang. William Kane là con một triệu phú nổi tiếng trên đất Mỹ, lớn lên trong nhung lụa của thế giới thượng lưu. Wladek Koskiewicz là đứa trẻ không rõ xuất thân, được gia đình người bẫy thú nhặt về nuôi. Một người được ấn định để trở thành chủ ngân hàng khi lớn lên, người kia nhập cư vào Mỹ cùng đoàn người nghèo khổ. \n" +
                        "\n" +
                        "Trải qua hơn sáu mươi năm với biết bao biến động, hai con người giàu hoài bão miệt mài xây dựng vận mệnh của mình . “Hai số phận” nói về nỗi khát khao cháy bỏng, những nghĩa cử, những mối thâm thù, từng bước đường phiêu lưu, hiện thực thế giới và những góc khuất... mê hoặc người đọc bằng ngôn từ cô đọng, hai mạch truyện song song được xây dựng tinh tế từ những chi tiết nhỏ nhất.) ",
                "AZA MƯỜI SÁU TUỔI, KHÔNG BAO GIỜ CÓ Ý ĐỊNH truy tìm ngài tỉ phú Russell Pickett tự nhiên biến mất đầy bí ẩn, chẳng qua là vì số tiền thưởng một trăm ngàn đô-la và cô bạn Daisy Thân Nhất và Chẳng Ngán Gì, nên háo hức bước vào cuộc điều tra. Thế là cùng nhau, đôi bạn trẻ xóa nhòa cự-ly-ngắn-mà-khoảng-cách-lớn ngăn cả hai với Davis, con trai của ngài tỉ phú Russell Pickett. Aza đang nỗ lực. Cô nỗ lực trở thành đứa con ngoan, người bạn tốt, học sinh giỏi, và có lẽ là một thám tử kiệt xuất nữa, trong lúc phải vật lộn với mớ suy nghĩ bòng bong trong đầu mình. Trở lại sau một thời gian dài được-chờ-đợi, John Green, tác giả của Đi tìm Alaska và Khi lỗi thuộc về những vì sao nhận được nhiều lời ngợi khen và giải thưởng cao quý, đã chia sẻ câu chuyện về Aza bằng một lối kể tỉnh bơ nhưng đầy sức công phá, trong một tiểu thuyết sinh động về tình yêu, tính kiên cường và sức mạnh của tình bạn mãi mãi"


        };
        String[] category={"Tình cảm","Tiểu thuyết","Tiểu thuyết","tiểu thuyết","tiểu thuyết"};
        int[]img={R.drawable.sach1,R.drawable.sach2,R.drawable.sach3,R.drawable.sach5,R.drawable.hinh7};
        int[]price={1000000,1230000,23400000,148500,137000};
        for (int i=0;i<img.length;i++){
            Book book=new Book(name[i],describe[i],category[i],img[i],price[i]);
            arrayList.add(book);
        }
        adapter = new book_Adapter(this,R.layout.produc_item, arrayList);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent item = new Intent( product_list.this, productDetail.class);
                item.putExtra("name",name[position]);
                item.putExtra("describe",describe[position]);
                item.putExtra("category",category[position]);
                item.putExtra("img",img[position]);
                item.putExtra("price",price[position]);
                startActivity(item);
            }
        });

    }

}